<?php
/**
 * This file is part of the lafayette-anticipations package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 13/04/2017
 */

namespace ReSourceAdapter\Helpers;

class Cursor {
  static function getPageInfoFragment(){
    return <<<QL
pageInfo{
  hasNextPage
  hasPreviousPage 
  startCursor
  endCursor
}
QL;
  }
}
