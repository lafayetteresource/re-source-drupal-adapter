<?php
/**
 * This file is part of the lafayette-anticipations package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 13/04/2017
 */

namespace ReSourceAdapter\Helpers;


class ListQueryParams {
  public $first;
  public $after;
  public $qs;
  public $sortBy;
  public $sortDirection;
  public $filters;

  /**
   * Get ListQueryParams from an associative array
   * @param $args
   * @return \ReSourceAdapter\Helpers\ListQueryParams
   */
  static function fromArray($args) {
    $params = new ListQueryParams();

    foreach ($args as $property => $value){
      if ($property == 'after' && isset($value)) {
        $params->{$property} = base64_encode("arrayconnection:$value");
      } else {
        $params->{$property} = $value;
      }
    }

    return $params;
  }

  /**
   * Add a filter.
   *
   * @param $filter
   */
  public function addFilter($filter){
    if (!isset($this->filters)){
      $this->filters = [];
    }

    $this->filters[] = $filter;
  }

  public function graphQLize(){
    $selectedParams = [];

    foreach (get_object_vars($this) as $property => $value) {
      if(!empty($value)) {
        $selectedParams[] = "$property: " . json_encode($value);
      }
    }

    return join(', ', $selectedParams);
  }
}
