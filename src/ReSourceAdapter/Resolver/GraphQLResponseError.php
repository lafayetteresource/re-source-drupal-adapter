<?php
/**
 * This file is part of the lafayette-anticipations package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 26/04/2017
 */

namespace ReSourceAdapter\Resolver;

use Throwable;

class GraphQLResponseError extends \Exception {
  protected $errors = [];

  public function __construct($errors) {
    $this->errors = $errors;

    $errorMessages = [];

    foreach ($this->errors as $index => $error){
      $errorMessages[] = ($index+1) . ". ${error['message']}";
    }

    $message = join("\n", $errorMessages);

    $this->message = <<<ERR
GraphQL endpoint returns following errors:

$message
ERR;
  }
}
