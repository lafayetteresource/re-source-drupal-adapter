<?php
/**
 * This file is part of the Re-Source adapter for Drupal package.
 *
 * Developped by Mnemotix <mathieu.rogelja@mnemotix.com>
 *
 * Date : 07/04/2017
 */
namespace ReSourceAdapter\Model;

use ReSourceAdapter\Helpers\Fragment;
use ReSourceAdapter\Helpers\ListQueryParams;

class Exhibition extends ModelAbstract {
  /** @var string Exhibition title */
  protected $title;

  /** @var string Exhibition description */
  protected $description;

  /** @var string Exhibition short description */
  protected $shortDescription;

  /** @var \ReSourceAdapter\Model\Actor[] Exhibition authors */
  protected $authors = [];

  /** @var \ReSourceAdapter\Model\Event[] Exhibition actors */
  protected $events = [];

  /** @var  double Exhibition start date  */
  protected $startDate;

  /** @var  double Exhibition end date */
  protected $endDate;

  /** @var \ReSourceAdapter\Model\Resource[] Exhibition pictures */
  protected $pictures = [];

  /** @var \ReSourceAdapter\Model\Resource Exhibition credits */
  protected $mainPicture;

  /** @var string Project facebook Uri */
  protected $facebookUri;

  /** @var string Project twitter Uri */
  protected $twitterUri;

  /** @var string Project ticketing Uri */
  protected $ticketingUri;

  /** @var \ReSourceAdapter\Model\Involvement[] Project involvements */
  protected $involvements = [];

  /** @var \ReSourceAdapter\Model\Artwork[] Project artworks */
  protected $artworks = [];

  /**
   * @return \ReSourceAdapter\Model\Event[]
   */
  public function getEvents() {
    return $this->events;
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @return string
   */
  public function getShortDescription() {
    return $this->shortDescription;
  }

  /**
   * @return \ReSourceAdapter\Model\Actor[]
   */
  public function getAuthors() {
    return $this->authors;
  }

  /**
   * @return \ReSourceAdapter\Model\Resource[]
   */
  public function getPictures() {
    return $this->pictures;
  }

  /**
   * @return \ReSourceAdapter\Model\Resource
   */
  public function getMainPicture() {
    return $this->mainPicture;
  }

  /**
   * @return float
   */
  public function getStartDate() {
    return $this->startDate;
  }

  /**
   * @return float
   */
  public function getEndDate() {
    return $this->endDate;
  }

  /**
   * @return string
   */
  public function getFacebookUri() {
    return $this->facebookUri;
  }

  /**
   * @return string
   */
  public function getTwitterUri() {
    return $this->twitterUri;
  }

  /**
   * @return string
   */
  public function getTicketingUri() {
    return $this->ticketingUri;
  }

  /**
   * @return \ReSourceAdapter\Model\Involvement[]
   */
  public function getInvolvements() {
    return $this->involvements;
  }

  /**
   * @return \ReSourceAdapter\Model\Artwork[]
   */
  public function getArtworks() {
    return $this->artworks;
  }

  /**
   * Get extra fragment
   * @param $fragmentName
   * @return string
   */
  static function getFragment($fragmentName){
    $authorFragmentName = Fragment::generateName();
    $authorFragment = Actor::getFragment($authorFragmentName);

    $resourceFragmentName = Fragment::generateName();
    $resourceFragment = Resource::getFragment($resourceFragmentName);

    $projectInvolvementFragmentName = Fragment::generateName();
    $projectInvolvementFragment = Involvement::getFragment($projectInvolvementFragmentName);

    $artworkFragmentName = Fragment::generateName();
    $artworkFragment = Artwork::getFragment($artworkFragmentName);

    return <<<GRAPHQL
fragment $fragmentName on ExhibitionObject{
  id
  title
  description
  shortDescription
  creationDate
  lastUpdate
  startDate
  endDate
  pictures {
    edges {
      resource: node {
        ...$resourceFragmentName
      }
    }
  }
  mainPicture {
    ...$resourceFragmentName
  }
  authors{
    edges{
      actor: node{
        ...$authorFragmentName
      }
    }
  }
  project{
    involvements{
      edges{
        involvement: node{
          ...$projectInvolvementFragmentName
        }
      }
    }
    externalLinks{
      edges{
        externalLink: node{
          name
          link
        }
      }
    }
  }
  artworks{
    edges{
      artwork: node{
        ...$artworkFragmentName
      }
    }
  }
}

$authorFragment
$resourceFragment
$projectInvolvementFragment
$artworkFragment
GRAPHQL;
  }

  /**
   * Get exhibition list GraphQL query.
   *
   * @param \ReSourceAdapter\Helpers\ListQueryParams $args
   * @return string
   */
  static function getListQuery(ListQueryParams $args){
    $fragmentName = Fragment::generateName();
    $fragment = Exhibition::getFragment($fragmentName);

    return <<<GRAPHQL
query{
  exhibitions({$args->graphQLize()}){
    edges{
      exhibition: node{
        ...$fragmentName
      }
    }
  }
}

$fragment

GRAPHQL;
  }

  /**
   * Return a list of exhibitions from a GraphQL response.
   *
   * @param array $data
   * @return \ReSourceAdapter\Model\Exhibition[]
   */
  static function fromListResponse(array $data){
    $exhibitions = [];

    foreach ($data['exhibitions']['edges'] as $exhibitionsData) {
      $exhibitions[] = Exhibition::fromResponse($exhibitionsData);
    }

    return $exhibitions;
  }


  /**
   * Get exhibition from GraphQL response data.
   *
   * @param $data
   * @return \ReSourceAdapter\Model\Exhibition
   */
  static function fromResponse($data) {
    $data = $data['exhibition'];

    $exhibition = new Exhibition();

    foreach ($data as $property => $value) {
      switch ($property) {
        case 'events':
          $exhibition->events = [];

          if(isset($value)) {
            foreach ($value['edges'] as $eventNode){
              $exhibition->events[] = Event::fromResponse($eventNode);
            }
          }
          break;

        case 'authors':
          $exhibition->authors = [];
          if(isset($value)) {
            foreach ($value['edges'] as $authorNode){
              $exhibition->authors[] = Actor::fromResponse($authorNode);
            }
          }
          break;

        case 'pictures':
          $exhibition->pictures = [];

          if(isset($value)) {
            foreach ($value['edges'] as $pictureNode){
              $exhibition->pictures[] = Resource::fromResponse($pictureNode);
            }
          }
          break;

        case 'artworks':
          $exhibition->artworks = [];

          if(isset($value)) {
            foreach ($value['edges'] as $artworkNode){
              $exhibition->artworks[] = Artwork::fromResponse($artworkNode);
            }
          }
          break;

        case 'mainPicture':
          if(isset($value)){
            $exhibition->mainPicture = Resource::fromResponse(["resource" => $value]);
          }
          break;

        case 'project':
          $project = $value;

          if (isset($project)) {
            foreach ($project as $projectProperty => $projectValue) {
              switch ($projectProperty) {
                case 'involvements':
                  $exhibition->involvements = [];
                  if (isset($projectValue)) {
                    foreach ($projectValue['edges'] as $actorNode) {
                      $exhibition->involvements[] = Involvement::fromResponse($actorNode);
                    }
                  }
                  break;

                case 'externalLinks':
                  if (isset($projectValue)) {
                    foreach ($projectValue['edges'] as $externalLinkNode) {
                      $externalLink = ExternalLink::fromResponse($externalLinkNode);

                      switch ($externalLink->getName()) {
                        case 'facebook':
                          $exhibition->facebookUri = $externalLink->getLink();
                          break;
                        case 'twitter':
                          $exhibition->twitterUri = $externalLink->getLink();
                          break;
                        case 'ticketing':
                          $exhibition->ticketingUri = $externalLink->getLink();
                          break;
                      }
                    }
                  }
                  break;
              }
            }
          }
          break;

        default:
          $exhibition->{$property} = $value;
      }
    }

    return $exhibition;
  }

  /**
   * Get exhibition GraphQL query.
   *
   * @param $exhibitionId
   * @param string $projectAlias
   * @return string
   */
  static function getQuery($exhibitionId){
    $fragmentName = Fragment::generateName();
    $fragment = Exhibition::getFragment($fragmentName);

    return <<<GRAPHQL
query{
  exhibition: artisticObject(artisticObjectId:"$exhibitionId") {
    ...$fragmentName
  }
}

$fragment
GRAPHQL;
  }

  /**
   * @return array
   */
  public function jsonSerialize() {
    $project = parent::jsonSerialize();

    return array_merge($project, [
      'id' => $this->getId(),
      'title' => $this->getTitle(),
      'description' => $this->getDescription(),
      'shortDescription' => $this->getShortDescription(),
      'startDate' => $this->getStartDate(),
      'endDate' => $this->getEndDate(),
      'creationDate' => $this->getCreationDate(),
      'lastUpdate' => $this->getLastUpdate(),
      'facebookUri' => $this->getFacebookUri(),
      'twitterUri' => $this->getTwitterUri(),
      'ticketingUri' => $this->getTicketingUri(),
      'artworks' => array_map(function($artwork){return $artwork->jsonSerialize(); }, $this->getArtworks()),
      'pictures' => array_map(function($resource){return $resource->jsonSerialize(); }, $this->getPictures()),
      'mainPicture' => $this->getMainPicture() ? $this->getMainPicture()->jsonSerialize() : null,
      'involvements' => array_map(function($involvement){return $involvement->jsonSerialize(); }, $this->getInvolvements()),
      'authors' => array_map(function($author){return $author->jsonSerialize(); }, $this->getAuthors()),
      'events' => array_map(function($event){return $event->jsonSerialize(); }, $this->getEvents()),
    ]);
  }
}
